@ECHO OFF

REM %~dp0 == Directory Path of that executing Batch file
SET ROOT_DIR=%~dp0\..
SET IR_EXE=%ROOT_DIR%\ironruby\bin\ir.exe
SET INNO_EXE=%ROOT_DIR%\InnoSetup\ISCC.exe

REM create PuraConfigTool.dat from AssemblyInfo.cs
%IR_EXE% %~dp0\build_installer.rb

REM build setup.exe
%INNO_EXE% %~dp0\ea_launcher.iss
