require 'fileutils'

#SVN_REVISION_PATHNAME     = File.join(File.dirname(__FILE__), '../svn_revision.cs')
info_data_PATHNAME    = File.join(File.dirname(__FILE__), '../ea_launcher/Properties/AssemblyInfo.cs')
INNO_SCRIPT_INCL_PATHNAME = File.join(File.dirname(__FILE__), 'ea_launcher.dat')

info_data = {}

#File.open(SVN_REVISION_PATHNAME).each_line do |line|
  # md = line.match(/public const string REVISION = \"(.+)\";/)
  # info_data["SvnRevisionAll"] = md[1] unless md.nil?
  # info_data["SvnRevision"] = md[1][/\d+/] unless md.nil?   # skip modifiers

  # md = line.match(/public const string PATH     = \"(.+)\";/)
  # info_data["SvnPath"] = md[1]  unless md.nil?
# end

info_data["SvnRevision"] = 0


File.open(info_data_PATHNAME).each_line do |line|
  md = line.match(/^\[assembly:\s+AssemblyTitle\("(.+)"\)\]$/)
  info_data["AssemblyTitle"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyDescription\("(.+)"\)\]$/)
  info_data["AssemblyDescription"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyConfiguration\("(.+)"\)\]$/)
  info_data["AssemblyConfiguration"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyCompany\("(.+)"\)\]$/)
  info_data["AssemblyCompany"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyProduct\("(.+)"\)\]$/)
  info_data["AssemblyProduct"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyCopyright\("(.+)"\)\]$/)
  info_data["AssemblyCopyright"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyTrademark\("(.+)"\)\]$/)
  info_data["AssemblyTrademark"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyCulture\("(.+)"\)\]$/)
  info_data["AssemblyCulture"] = md[1]  unless md.nil?

  md = line.match(/^\[assembly:\s+Guid\("(.+)"\)\]$/)
  info_data["Guid"] = md[1] unless md.nil?

  md = line.match(/^\[assembly:\s+AssemblyVersion\("(.+)"\)\]$/)
  info_data["AssemblyVersion"] = md[1][/\d+\.\d+\.\d+/]  unless md.nil?   # 2.0.0.0 -> 2.0.0

  md = line.match(/^\[assembly:\s+AssemblyFileVersion\("(.+)"\)\]$/)
  info_data["AssemblyFileVersion"] = md[1]  unless md.nil?
end

File.open(INNO_SCRIPT_INCL_PATHNAME, 'w') do |fh|
  fh.puts "#define MyAppVersion \"#{info_data["AssemblyVersion"]}\""                                            # -> #define MyAppVersion "2.0.1"
  fh.puts "#define MyAppName \"#{info_data["AssemblyProduct"]}\""                                               # -> #define MyAppName "PuraConfigTool"
  fh.puts "#define MyAppVerName \"#{info_data["AssemblyProduct"]} #{info_data["AssemblyVersion"]}\""            # -> #define MyAppName "PuraConfigTool 2.0.0"
  fh.puts "#define MyAppExeName \"#{info_data["AssemblyProduct"]}.exe\""                                        # -> #define MyAppExeName "PuraConfigTool.exe"
  fh.puts "#define MyAppPublisher \"#{info_data["AssemblyCompany"]}\""                                          # -> #define MyAppPublisher "Solcept AG"
  fh.puts "#define MyAppCopyright \"#{info_data["AssemblyCopyright"]}\""                                        # -> #define MyAppCopyright "Franke CS"
  fh.puts "#define MyVersionInfoVersion \"#{info_data["AssemblyVersion"]}.#{info_data["SvnRevision"]}\""        # -> #define MyVersionInfoVersion "2.0.0.9877"
  fh.puts "#define MyDescription \"#{info_data["AssemblyDescription"]}\""                                       # -> #define MyDescription "ead, write, update Pura configuration data sets"
#  fh.puts "#define MySvnRevisionAll \"#{info_data["SvnRevisionAll"]}\""                                         # -> #define MyAppCopyright "9882_M"
#  fh.puts "#define MySvnRevision \"#{info_data["SvnRevision"]}\""                                               # -> #define MyAppCopyright "9882"
#  fh.puts "#define MySvnPath \"#{info_data["SvnPath"]}\""                                                       # -> #define MySvnPath "/svn/trunk"
  fh.puts "#define MyGuid \"#{info_data["Guid"]}\""                                                             # -> #define MyGuid "4d11aaa1-83d7-42c1-8e4e-e2a4a1b1754d"
end
