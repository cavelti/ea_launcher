﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Reflection;


namespace ea_launcher
{
  class Program
  {
    static void Main(string[] args)
    {
      //--- check command line parameters -------------------------------------
      if (args.Length == 0)
      { // Missing command line parameter -> exit application
        ExitWithMessage(string.Format("Missing parameter!\nUsage: {0} example.eap", Assembly.GetEntryAssembly().GetName().Name));
      }

      var eaProjectPathName = args[0];
      if (isFileExisting(eaProjectPathName) == false)
      { // EA project file not found -> exit application
        ExitFileNotFound(eaProjectPathName);
      }


      //--- read launcher configuration ---------------------------------------
      string EXE_PATHNAME = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
      string CFG_PATHNAME = Path.Combine(EXE_PATHNAME, "config.json");

      if (isFileExisting(CFG_PATHNAME) == false)
      { // EA Launcher configuration not found -> exit application
        ExitFileNotFound(CFG_PATHNAME);      
      }

      Dictionary<string, string> config = null;
      string errorString = ReadConfiguration(CFG_PATHNAME, out config);

      if (errorString != "")
      { // Configuration parsing error -> exit application
        ExitWithMessage(string.Format("Configuration file error!\n({0})\n\n{1}", CFG_PATHNAME, errorString));
      }


      //--- launch EA or EA light depending on project file read-only flag ----
      var     eaPathName       = config["ea_pathname"];
      var     eaLightPathName  = config["ea_light_pathname"];
      string  progPathName     = "";

      if (IsFileReadOnly(eaProjectPathName))
      {
        progPathName = eaLightPathName;
      }
      else
      {
        progPathName = eaPathName;
      }

      if (isFileExisting(progPathName))
      {
        errorString = LaunchEa(progPathName, eaProjectPathName);

        if (errorString == "")
        { // EA launch successful
        }
        else
        { // EA launching failed -> exit application
          ExitWithMessage(errorString);
        }
      }
      else
      { // EA program not found -> exit application
        string extraMessage = string.Format("\nConfigured EA program not found!\n(check and correct settings in {0})", CFG_PATHNAME);

        ExitFileNotFound(progPathName, extraMessage);
      }
    }


    private static string ReadConfiguration(string pathName, out Dictionary<string, string> config)
    {
      string dataString   = File.ReadAllText(pathName);
      string errorString  = "";

      try
      {
        config = JsonConvert.DeserializeObject<Dictionary<string, string>>(dataString);
      }
      catch (System.Exception ex)
      {
        config      = null;
        errorString = ex.Message;
      }

      return errorString;
    }


    // Returns whether a file is existing. 
    private static bool isFileExisting(string pathName)
    {
      // Create a new FileInfo object.
      FileInfo fInfo = new FileInfo(pathName);

      // Return the IsReadOnly property value. 
      return fInfo.Exists;
    }


    // Returns whether a file is read-only. 
    private static bool IsFileReadOnly(string pathName)
    {
      // Create a new FileInfo object.
      FileInfo fInfo = new FileInfo(pathName);

      // Return the IsReadOnly property value. 
      return fInfo.IsReadOnly;
    }


    private static string LaunchEa(String progPathName, String filePathName)
    {
      Process process                           = new Process();

      process.StartInfo.FileName                = quoteString(progPathName);
      process.StartInfo.Arguments               = quoteString(filePathName);
      process.StartInfo.CreateNoWindow          = true;
      process.StartInfo.ErrorDialog             = true;

      string errorString = "";

      try
      {
        if (process.Start() == false)
        {
          errorString = string.Format("Can't launch EA!\n({0} {1})", progPathName, filePathName);
        }
      }
      catch (System.Exception ex)
      {
        errorString = ex.Message;
      }

      return errorString;
    }


    private static string quoteString(string aString)
    {
      return "\"" + aString + "\"";
    }


    private static void ExitWithMessage(string message)
    {
      string caption = "EA Launcher";

      MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
      Exit();
    }


    private static void ExitFileNotFound(string pathName, string extraMessage = "")
    {
      ExitWithMessage(string.Format("File not found!\n({0})\n{1}", pathName, extraMessage));
    }


    private static void Exit(int exitCode = -1)
    {
      System.Environment.Exit(exitCode);
    }
  }
}
